name := "bcgdv"

version := "1.0"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.12.2" % "test"
libraryDependencies += "com.github.nscala-time" %% "nscala-time" % "1.8.0" % "test"

mainClass in (Compile, run) := Some("experiments.Calculator")

scalaVersion := "2.10.4"

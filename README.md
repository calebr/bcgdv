This repo contains solution to the BCG Digital Ventures interview code challenge.

Problem
-----

You have joined a science project as the latest team member. Scientists on the project are running a series of
experiments and need to calculate the number of full days elapsed in between the experiment’s start and end dates,
i.e. the first and the last day are considered partial days and never counted. Following this logic, an experiment
that has run from 07/11/1972 and 08/11/1972 should return 0, because there are no fully elapsed days contained
in between those dates, and 01/01/2000 to 03/01/2000 should return 1. The solution needs to cater for all valid
dates between 01/01/1901 and 31/12/2999.

As the new guy on the team, you are tasked with the job. The scientists are using an older, command line based
system and need at least one way of providing input and output on the terminal - they’re asking you to build the
system accordingly. They also don’t just take your word for it when it comes to saying “i’m done”, they want proof. It
turns out they have designed the job as an experiment and given you a few test cases to pass and validate the output
of your program.

Solution
-----

Although main problem in this challenge concerns dates, I believe we shouldn't create any rich date objects.
Implementation of the comprehensive date object manipulations would be difficult and too time consuming for this exercise.
Instead we should use minimal representation of the date (day, month, year) and make it easily replaceable with objects
from other date/time library once more functionality is needed.

In order to use different date object, one have to provide implementation for the following traits:

* DateParser - specifies how convert strings into valid date objects
* DatesDifference - specifies how to compare date objects

Basic implementations are provided:

* RegexDateParser - which parses date using scala regex parser combinators
* ExperimentsDaysCounter - which uses Julian Day Number algorithm

Correct results are verified with property tests.

Effort was made to make this code concise, after all this problem is solvable with one line of code.

How to run
----

Type `sbt "run -h"` for usage.

Type `sbt test` to run tests.

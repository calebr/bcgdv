package experiments.dates

import experiments.dates.ExperimentsDaysCounter._
import org.joda.time.{Days, LocalDate}
import org.scalacheck.Prop._
import org.scalacheck.{Arbitrary, Gen, Properties}

class ExperimentsDaysCounterSpec extends Properties("ExperimentsDaysCounter") {

  // Random date between 01/01/1901 and 31/12/2999
  implicit lazy val experimentDate : Arbitrary[LocalDate] = Arbitrary(
    for { ms <- Gen.choose(-2177492400000l, 32503554000000l) } yield new LocalDate(ms))

  def asExperimentDate(date: LocalDate) : ExperimentDate =
    ExperimentDate(date.getDayOfMonth, date.getMonthOfYear, date.getYear)

  def differenceUsingJodaDate(a: LocalDate, b: LocalDate) : Int =
    if (a.isBefore(b))
      Math.max(Days.daysBetween(a, b).getDays - 1, 0)
    else
      Math.max(Days.daysBetween(b, a).getDays - 1, 0)

  def checkDifference(a: LocalDate, b: LocalDate) =
    if (a.isBefore(b))
      difference(asExperimentDate(a), asExperimentDate(b))
    else
      difference(asExperimentDate(b), asExperimentDate(a))

  property("the same answer as using joda dates") = forAll { (a: LocalDate, b: LocalDate) =>
    checkDifference(a, b) == differenceUsingJodaDate(a, b)
  }

  property("returns non negative numbers") = forAll { (a: LocalDate, b: LocalDate) =>
    checkDifference(a, b) >= 0
  }

  property("returns zero for the same start and finish date") = forAll { (a: LocalDate) =>
    checkDifference(a, a) == 0
  }

  property("sanity check for test case: 02/06/1983 - 22/06/1983") = forAll { _ : LocalDate =>
    checkDifference(new LocalDate(1983, 6, 2), new LocalDate(1983, 6, 22)) == 19
  }

  property("sanity check for test case: 04/07/1984 - 25/12/1984") = forAll { _ : LocalDate =>
    checkDifference(new LocalDate(1984, 7, 4), new LocalDate(1984, 12, 25)) == 173
  }

  property("sanity check for test case: 03/01/1989 - 03/08/1983") = forAll { _ : LocalDate =>
    checkDifference(new LocalDate(1983, 8, 3), new LocalDate(1989, 1, 3)) == 1979
  }

  property("sanity check for reference implementation: 02/06/1983 - 22/06/1983") = forAll { _ : Unit =>
    differenceUsingJodaDate(new LocalDate(1983, 6, 2), new LocalDate(1983, 6, 22)) == 19
  }

  property("sanity check for reference implementation: 04/07/1984 - 25/12/1984") = forAll { _ : Unit =>
    differenceUsingJodaDate(new LocalDate(1984, 7, 4), new LocalDate(1984, 12, 25)) == 173
  }

  property("sanity check for reference implementation: 03/01/1989 - 03/08/1983") = forAll { _ : Unit =>
    differenceUsingJodaDate(new LocalDate(1983, 8, 3), new LocalDate(1989, 1, 3)) == 1979
  }
}
package experiments.dates

import org.joda.time.LocalDate
import org.scalacheck.Prop._
import org.scalacheck.{Arbitrary, Gen, Properties}

class RegexDateParserSpec extends Properties("RegexDateParser") {

  // Random date between 01/01/1901 and 31/12/2999
  implicit lazy val experimentDate : Arbitrary[LocalDate] = Arbitrary(
    for { ms <- Gen.choose(-2177492400000l, 32503554000000l) } yield new LocalDate(ms))

  def invalidDates : Gen[String] = Gen.oneOf(
    "40/05/2015", "00/05/2015", "-12/05/2015",
    "22/13/2015", "22/00/2015", "22/-10/2015",
    "12/03/-1000", "31/04/2015", "29/02/2015"
  )

  val dateFormat = "dd/MM/yyyy"

  property("accepts all valid dates") = forAll { (d: LocalDate) =>
    RegexDateParser(d.toString(dateFormat)) match {
      case Some(pd) =>
        pd.day == d.getDayOfMonth &&
        pd.month == d.getMonthOfYear &&
        pd.year == d.getYear
      case None => false
    }
  }

  property("rejects random input") = forAll { (s: String) =>
    RegexDateParser(s).isEmpty
  }

  property("rejects invalid dates") = forAll (invalidDates) { (s: String) =>
    RegexDateParser(s).isEmpty
  }
}

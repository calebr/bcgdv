package experiments

import scala.annotation.tailrec

object ArgumentsParser {

  type ArgumentsMap = Map[String, String]

  val emptyArgumentsMap = Map[String, String]()

  @tailrec
  def parse(args: List[String], map: ArgumentsMap = emptyArgumentsMap) : Option[ArgumentsMap] =
    args match {
      case ("-f" | "--date-from") :: v :: t => parse(t, map.updated("--date-from", v))
      case ("-t" | "--date-to")   :: v :: t => parse(t, map.updated("--date-to", v))
      case Nil                              => Some(map)
      case _                                => None
    }

  def apply(args: Array[String], required: Set[String]) = {
    parse(args.toList, emptyArgumentsMap) match {
      case r@Some(map) if required subsetOf map.keys.toSet => r
      case _ => None
    }
  }
}

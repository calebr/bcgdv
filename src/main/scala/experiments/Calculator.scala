package experiments

import experiments.dates.ExperimentsDaysCounter.difference
import experiments.dates.RegexDateParser

object Calculator {
  def help =
    """
      | Usage: ./calculator [Options]
      |
      | Options:
      |   -f <date>   --date-from <date>      start date of the experiment
      |   -t <date>   --date-to <date>        finish date of the experiment
      |   -h          --help                  print this screen
      |
      | Provide dates in format DD/MM/YYYY
    """.stripMargin

  val requiredArgs = Set("--date-from", "--date-to")

  def main(args: Array[String]) =
    calculate(args) match {
      case Some(difference) => println(difference)
      case _ => println(help)
    }

  def calculate(args: Array[String]) =
    for {
      args    <- ArgumentsParser(args, requiredArgs)
      dateA   <- RegexDateParser(args("--date-from"))
      dateB   <- RegexDateParser(args("--date-to"))
    } yield
      if (dateA.isBefore(dateB))
        difference(dateA, dateB)
      else
        difference(dateB, dateA)
}

package experiments.dates

trait DateParser[A] {
  def apply(input: String): Option[A]
}
package experiments.dates

object ExperimentsDaysCounter extends DatesDifference[ExperimentDate] {

  // Count days using Julian Day Number algorithm.
  // Source: http://en.wikipedia.org/wiki/Julian_day
  object JulianDayNumber {
    def apply(date: ExperimentDate) : Int = {
      val a = date.month - 14
      date.day - 32075 + 1461 * (date.year  + 4800 + a / 12) / 4 +
        367 * (date.month - 2 - a / 12 * 12) / 12 - 3 * ((date.year + 4900 + a / 12) / 100) / 4
    }
  }

  override def difference(from: ExperimentDate, to: ExperimentDate) =
    Math.max(JulianDayNumber(to) - JulianDayNumber(from) - 1, 0)
}

package experiments.dates

import scala.util.parsing.combinator.RegexParsers

object RegexDateParser extends RegexParsers with DateParser[ExperimentDate] {

  def day   = """\d{2}""".r ^^ { _.toInt }
  def month = """\d{2}""".r ^^ { _.toInt }
  def year  = """\d{4}""".r ^^ { _.toInt }

  def experimentDate : Parser[ExperimentDate] =
    day ~ "/" ~ month ~ "/" ~ year ^^ {
      case (d ~ _ ~ m ~ _ ~ y) => ExperimentDate(d, m, y)
    }

  override def apply(input: String) : Option[ExperimentDate] = {
    parseAll(experimentDate, input) match {
      case Success(date, _) if date.isValid() => Some(date)
      case _ => None
    }
  }
}

package experiments.dates

case class ExperimentDate(day: Int, month: Int, year: Int) {
  def isBefore(otherDate: ExperimentDate): Boolean =
    year <= otherDate.year && month <= otherDate.month && day < otherDate.day

  def isValid() = {
    def isLeapYear(year: Int) =
      if (year % 400 == 0) true
      else if (year % 100 == 0) false
      else if (year % 4 == 0) true
      else false

    def daysInMonth(month: Int, year: Int) = {
      val days = List(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
      val d = days(month - 1)
      if (isLeapYear(year) && month == 2) d + 1 else d
    }

    Range(1901, 3000).contains(year) &&
    Range(1, 13).contains(month) &&
    Range(1, daysInMonth(month, year) + 1).contains(day)
  }
}

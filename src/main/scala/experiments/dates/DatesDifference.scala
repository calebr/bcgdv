package experiments.dates

trait DatesDifference[A] {
  def difference(dateA: A, dateB: A) : Int
}

